from __future__ import unicode_literals

from django.apps import AppConfig


class AskmeAppConfig(AppConfig):
    name = 'askme_app'
