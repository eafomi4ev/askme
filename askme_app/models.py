# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User


class QuestionManager(models.Manager):
    def current(self, questionId):
        return self.filter(id=questionId)

    def new(self):
        if (self.count() - 30 >= 0):
            return self.all().order_by('-publicationDate')[:50]
        else:
            return self.all()

    def getHotQuestions(self):
        list = self.annotate(countLikes=models.Count('questionlike__question_id')).order_by('-countLikes')[:5]
        # idList = []
        # for question in list:
        #     idList.append("%s " % question.id)
        # raise Exception(idList)
        return list

    def getQuestionsByTag(self, tagName):
        # raise Exception(Question.objects.filter(tag__name=tagName))
        return Question.objects.filter(tag__name=tagName)

class AnswerManager(models.Manager):
    def getQuestionAnswers(self, question_id):
        return self.filter(question_id=question_id)


class Question(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Заголовок')
    text = models.TextField(verbose_name=u'Текст вопроса')
    user = models.ForeignKey(User, default=0, verbose_name='ID юзера')
    publicationDate = models.DateTimeField(auto_now_add=True, verbose_name=u'Дата публикации')
    tag = models.ManyToManyField('Tag', verbose_name=u'Тэги')

    objects = QuestionManager()

    class Meta:
        verbose_name = u'Вопрос'
        verbose_name_plural = u'Вопросы'

    def __unicode__(self):
        return self.title

    def ratio(self):
        return self.questionlike_set.filter(status=True).count() - self.questionlike_set.filter(status=False).count()


class Answer(models.Model):
    text = models.TextField(verbose_name=u'Текст ответа')
    publicationDate = models.DateTimeField(auto_now=True, verbose_name=u'Дата создания ответа')
    user = models.ForeignKey(User, default=0, verbose_name='ID юзера')
    question = models.ForeignKey('Question', on_delete=models.CASCADE, verbose_name='ID вопроса')
    objects = AnswerManager()

    class Meta:
        verbose_name = u'Ответ'
        verbose_name_plural = u'Ответы'

    def __unicode__(self):
        return self.text

    def ratio(self):
        return self.answerlike_set.filter(status=True).count() - self.answerlike_set.filter(status=False).count()


class Questionlike(models.Model):
    user = models.ForeignKey(User, verbose_name=' юзера')
    question = models.ForeignKey('Question', on_delete=models.CASCADE, verbose_name='Номер вопроса')
    status = models.BooleanField(verbose_name='Понравилась запись')

    # objects = QuestionlikeManager()

    class Meta:
        verbose_name = u'Лайк вопроса'
        verbose_name_plural = u'Лайки вопросов'

    def __unicode__(self):
        return 'like' if self.status else 'dislike'


class Answerlike(models.Model):
    user = models.ForeignKey(User, default=0, verbose_name='ID юзера')
    answer = models.ForeignKey('Answer', default=0, on_delete=models.CASCADE, verbose_name='ID ответа')
    status = models.BooleanField(verbose_name='Понравилась запись')

    class Meta:
        verbose_name = u'Лайк ответа'
        verbose_name_plural = u'Лайки ответов'

    def __unicode__(self):
        return 'like' if self.status else 'dislike'


class Tag(models.Model):
    name = models.CharField(max_length=50, verbose_name=u'Имя тэга')

    class Meta:
        verbose_name = u'Тэг'
        verbose_name_plural = u'Тэги'

    def __unicode__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=0, verbose_name='ID юзера')
    nick = models.CharField(blank=True, null=True, max_length=50,
                            verbose_name='Ник пользователя')  # убрать blank и null
    avatar = models.ImageField(blank=True, null=True, upload_to='static/img/avatars', default=None,
                               verbose_name='Аватар')

    class Meta:
        verbose_name = u'Профиль'
        verbose_name_plural = u'Профили'

    def __unicode__(self):
        return self.id
