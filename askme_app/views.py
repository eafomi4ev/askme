from datetime import datetime
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse, Http404
from askme_app.models import Question, Answer, Tag, User, Questionlike


# datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")

def post_get_view(request):
    res_template = '{0}  {1}' + ', '
    response_get = ''
    response_post = ''
    for key, value in request.GET.items():
        response_get += res_template.format(key, value)

    response_post = ''
    try:
        response_post = 'POST: ' + request.POST['login2']
    except KeyError:
        response_post = 'Net takih'
    return render(request, '../templates/getAndPostInDjango.html', {'data': iter([response_get, response_post])})


def paginate(objectsList, request):
    paginator = Paginator(objectsList, 10)  # Show 10 objects per page

    page = request.GET.get('page')
    try:
        response = paginator.page(page)
    except PageNotAnInteger:
        response = paginator.page(1)
    except EmptyPage:
        response = paginator.page(paginator.num_pages)

    return response


def index(request):
    questionList = Question.objects.new()
    return render(request, 'askme/index.html', {'questionList': paginate(questionList, request), })


def hotQuestions(request):
    hotQuestionsList = Question.objects.getHotQuestions()
    return render(request, 'askme/index.html', {'questionList': hotQuestionsList, })


def qustionsByTeg(request, tagName):
    # questionList = []
    # for number in range(1, 2):
    #     questionList.append({'id': number,
    #                          'title': 'QUESTION #%s' % number,
    #                          'text': 'QUESTION BODY and other text'})
    #
    # hotTagsList = ['sql', 'js', 'python', 'django', ]
    # countAnswers = 10

    questionList = Question.objects.getQuestionsByTag(tagName)

    return render(request, 'askme/index.html', {'questionList': questionList,
                                                #'hotTagsList': hotTagsList,
                                                #'countAnswers': countAnswers
                  })


def qustionByNumber(request, number):
    # raise Exception(request.get_raw_uri())
    try:
        questionList = Question.objects.current(number)
    except:
        return Http404
    answersList = Answer.objects.getQuestionAnswers(number)
    current_page = paginate(answersList, request)
    return render(request, 'askme/question.html', {'questionList': questionList,
                                                   'answersList': current_page, })


def login(request):
    return render(request, 'askme/login.html')


def signup(request):
    return render(request, 'askme/signup.html')


def ask(request):
    return render(request, 'askme/ask.html')


def base(request):
    return render(request, 'askme/base.html')


def settings(request):
    return render(request, 'askme/settings.html')
