# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from random import choice
from askme_app.models import User, Profile
from faker import Factory
from askme_app.models import Answerlike

class Command(BaseCommand):
    help = u"Заполнение БД тестовыми данными"

    def add_arguments(self, parser):
        parser.add_argument('-n',
                            action='store',
                            dest='number',
                            default=1,
                            )

    def handle(self, *args, **options):
        answer = Answerlike()
        answer.user_id = 5
        answer.answer_id = 1
        answer.status = True
        answer.save()