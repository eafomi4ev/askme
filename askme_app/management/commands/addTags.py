# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from askme_app.models import Tag
from faker import Factory


class Command(BaseCommand):
    help = u"Заполнение БД тестовыми данными"

    def handle(self, *args, **options):
        tags = [u'google', u'android', u'Google', u'linux', u'php', u'javascript', u'microsoft', u'apple', u'социальные',
                u'сети', u'стартапы', u'программирование', u'java', u'python', u'хабрахабр', u'Android', u'интернет']
        for x in tags:
            tag = Tag()
            tag.name = x
            tag.save()
            self.stdout.write('Created "%s" tag' % x)
