# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from random import choice, randint
from askme_app.models import Question, Tag, Answer, Questionlike, Answerlike
from django.contrib.auth.models import User
from datetime import datetime
from faker import Factory


class Command(BaseCommand):
    help = u"Заполнение БД тестовыми данными"

    def add_arguments(self, parser):
        parser.add_argument('-n',
                            action='store',
                            dest='number',
                            default=1,
                            )

    def handle(self, number, *args, **options):
        fake = Factory.create()
        number = int(number)
        users = User.objects.all()

        for x in range(number):
            # Добавление вопроса
            question = Question()
            question.title = fake.sentence(nb_words=randint(4, 8), variable_nb_words=True)
            question.text = fake.text(max_nb_chars=randint(500, 1500))
            # Добавление автора к вопросу
            question.user_id = choice(users).id
            # question.publicationDate = datetime.strftime(datetime.now(), "%Y.%m.%d %H:%M:%S")
            tagslist = Tag.objects.all()
            question.save()
            # Добавление лайков и дислайков вопросу
            usersSet = set()
            i = 0
            count = randint(5, 30)
            while i < count:
                user_id = choice(User.objects.all()).id
                if user_id not in usersSet:
                    like = Questionlike()
                    like.user_id = user_id
                    like.question_id = question.id
                    like.status = choice([True, False])
                    like.save()
                    usersSet.add(user_id)
                    i += 1

            # Добавление тэгов к вопросу
            i = 0
            count = randint(1, 10)
            while i < count:
                question.tag.add(choice(tagslist).id)
                i += 1
            question.save()

            # Добавление ответов к вопросу
            i = 0
            count = randint(1, 30)
            while i < count:
                answer = Answer()
                answer.text = fake.text(max_nb_chars=randint(500, 1500))
                answer.user_id = choice(User.objects.all()).id
                answer.question_id = question.id
                answer.save()
                # Добавление лайков и дислайков ответу
                usersSet = set()
                j = 0
                count = randint(5, 15)
                while j < count:
                    user_id = choice(User.objects.all()).id
                    if user_id not in usersSet:
                        like = Answerlike()
                        like.user_id = user_id
                        like.answer_id = answer.id
                        like.status = choice([True, False])
                        like.save()
                        usersSet.add(user_id)
                    j += 1
                i += 1

            self.stdout.write('Created %s question with id = %s' % (x + 1, question.id))
