# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from askme_app.models import User, Profile
from faker import Factory


class Command(BaseCommand):
    help = u"Заполнение БД тестовыми данными"

    def add_arguments(self, parser):
        parser.add_argument('-n',
                            action='store',
                            dest='number',
                            default=1,
                            )

    def handle(self, *args, **options):
        fake = Factory.create()
        number = int(options['number'])
        for x in range(0, number):
            try:
                user = User()
                user.username = fake.user_name()
                user.first_name = fake.first_name()
                user.last_name = fake.last_name()
                user.email = fake.email()
                user.password = 'testpassword'
                user.is_active = True
                user.is_superuser = False
                user.save()

                profile = Profile()
                profile.user_id = user.id
                profile.nick = fake.user_name()
                profile.save()
                self.stdout.write('Created %s user' % (x + 1))
            except:
                continue
