from django.contrib import admin
from askme_app import models


# Register your models here.
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('title',)


class ProfileAdmin(admin.ModelAdmin):
    list_display = ('id',)


class TagAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')


admin.site.register(models.Question, QuestionAdmin)
admin.site.register(models.Profile, ProfileAdmin)
admin.site.register(models.Tag, TagAdmin)
