"""askme URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from askme_app import views

urlpatterns = [
    url(r'^get/', views.post_get_view, name='post_get_view'),
    url(r'^$', views.index, name='index'),
    url(r'^hot/', views.hotQuestions, name='hot'),
    url(r'^tag/(\w+)', views.qustionsByTeg, name='tag'),
    url(r'^question/(\d+)', views.qustionByNumber, name='question'),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^login/', views.login, name='login'),
    url(r'^ask/', views.ask, name='ask'),
    url(r'^base/', views.base, name='base'),
    url(r'^settings/', views.settings, name='settings'),
    url(r'^admin/', admin.site.urls, name='urls'),
    url(r'^page/(\d+)', views.index ),
]
